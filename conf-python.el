(use-package elpy)
(setq elpy-rpc-python-command "python3")
(setq python-shell-interpreter "python3")

(elpy-enable)

(use-package flycheck)
(when (require 'flycheck nil t)
  (setq elpy-modules (delq 'elpy-module-flymake elpy-modules))
  (add-hook 'elpy-mode-hook 'flycheck-mode))

;; Load ein, for Python notebooks
(use-package ein)

(setq ein:worksheet-enable-undo t)

