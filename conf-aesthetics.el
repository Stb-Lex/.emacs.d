;; Removes startup message
(setq inhibit-startup-message t)

;; Get and load Material theme
(use-package material-theme)
(load-theme 'material t) ;; load material theme

