;; Enable line enumeration for programming modes
(add-hook 'prog-mode-hook 'linum-mode)

;; Show paired parenthesis or other pairable characters
(setq show-paren-delay 0)
(show-paren-mode 1)

;; Function to toggle between commented/uncommented state of region or line
(defun comment-or-uncomment-region-or-line ()
  "Comments or uncomments the region or the current line if
there's no active region."
  (interactive)
  (let (beg end)
    (if (region-active-p)
        (setq beg (region-beginning) end (region-end))
      (setq beg (line-beginning-position) end (line-end-position)))
    (comment-or-uncomment-region beg end)))

;; Bind comment/uncomment toggle to M-;
(global-set-key (kbd "M-;") 'comment-or-uncomment-region-or-line)


;; Multi-term configuration
(use-package multi-term)

(global-set-key (kbd "C-c t") 'multi-term)
(global-set-key (kbd "C-c r") 'multi-term-dedicated-toggle)
(global-set-key (kbd "C-c s") 'multi-term-dedicated-select)

(setq multi-term-dedicated-skip-other-window-p t)
(setq multi-term-dedicated-select-after-open-p t)
(setq multi-term-dedicated-close-back-to-open-buffer-p t)

(setq multi-term-program "/bin/bash")
;; End Multi-term configuration

