;; Global org mode keys
(global-set-key (kbd "C-c l") 'org-store-link)
(global-set-key (kbd "C-c a") 'org-agenda)
(global-set-key (kbd "C-c c") 'org-capture)

;; Display nicer bullets
(use-package org-bullets
  :init
  (add-hook 'org-mode-hook 'org-bullets-mode))

;; Change the org-mode ellipsis into something nicer
(setq org-ellipsis "⤵")

;; Print time and note once TODO item is done
(setq org-log-done 'time)
(setq org-log-done 'note)

