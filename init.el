;; First setup package manager
(require 'package)

(add-to-list 'package-archives '("gnu" . "https://elpa.gnu.org/packages/") t)
(add-to-list 'package-archives '("melpa" . "http://melpa.org/packages/") t)
(add-to-list 'package-archives '("org" . "http://orgmode.org/elpa/") t)
(package-initialize)

(when (not (package-installed-p 'use-package))
  (package-refresh-contents)
  (package-install 'use-package))

(require 'use-package-ensure)
(setq use-package-always-ensure t)

;; Always compile packages
(use-package auto-compile
  :config (auto-compile-on-load-mode))

;; Always use the newest version of a package
(setq load-prefer-newer t)


;; Load configuration files
;; Miscellaneous
(load "~/.emacs.d/conf-misc.el")

;; Aesthetics
(load "~/.emacs.d/conf-aesthetics.el")

;; General programming configuration
(load "~/.emacs.d/conf-programming.el")

;; Python programming configuration
(load "~/.emacs.d/conf-python.el")

;; Org-mode configuration
(load "~/.emacs.d/conf-orgmode.el")


;; Start Emacs with God-mode on
(god-mode-all)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   (quote
    (org-bullets ein flycheck elpy multi-term material-theme god-mode auto-compile use-package))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
