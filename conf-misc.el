;; Change autosave folder
(setq backup-directory-alist `(("." . ,(concat user-emacs-directory "tmp"))))


;; Ask before closing Emacs
(setq confirm-kill-emacs 'yes-or-no-p)


;; Highlight current line
(global-hl-line-mode)


;; Enables ido mode everywhere
(setq ido-enable-flex-matching t)
(setq ido-everywhere t)
(ido-mode 1)


;; Selected text gets replaced by typed text.
(delete-selection-mode t)

(setq sentence-end-double-space nil
      highligt-tabs t
      show-trailing-withspace t)

(defalias 'yes-or-no-p 'y-or-n-p)

;; Saved file get appended an empty new line when there is none
(setq require-final-newline t)


;; Load buffer-move to be able to quickly swab buffer position
(load "~/.emacs.d/code/buffer-move.el")

;; Keybindings for buffer-move
(global-set-key (kbd "<C-S-up>")     'buf-move-up)
(global-set-key (kbd "<C-S-down>")   'buf-move-down)
(global-set-key (kbd "<C-S-left>")   'buf-move-left)
(global-set-key (kbd "<C-S-right>")  'buf-move-right)


;; Dired configuration
;; Human-readable units for file size
(setq-default dired-listing-switches "-alh")

;; End Dired configuration 


;; When a file gets changed elsewhere, refresh the buffer containing that file
(global-auto-revert-mode t)


;; Change default font to Anonymous Pro
(add-to-list 'default-frame-alist '(font . "Anonymous Pro-15" ))
(set-face-attribute 'default t :font "Anonymous Pro-15" )


;; Duplicate region, bound to C-c d
(load "~/.emacs.d/code/duplicate-region.el")
(global-set-key (kbd "C-c d") 'duplicate-line-or-region)


;; Greek letters. Available with M-g letter.
(load "~/.emacs.d/code/greek.el")


;; God-mode configuration
(use-package god-mode)
(require 'god-mode)

;; Bind escape as global god mode activation
(global-set-key (kbd "<escape>") 'god-mode-all)

;; Change cursor when in god mode
(defun my-update-cursor ()
  (setq cursor-type (if (or god-local-mode buffer-read-only)
                        'box
                      'bar)))

(add-hook 'god-mode-enabled-hook 'my-update-cursor)
(add-hook 'god-mode-disabled-hook 'my-update-cursor)

;; God mode behavior in isearch
(require 'god-mode-isearch)
(define-key isearch-mode-map (kbd "<escape>") 'god-mode-isearch-activate)
(define-key god-mode-isearch-map (kbd "<escape>") 'god-mode-isearch-disable)

;; Repeat bound to '.'
(define-key god-local-mode-map (kbd ".") 'repeat) 

;; End God-mode configuration


;; Neotree
(use-package neotree
  :ensure t)

(global-set-key (kbd "C-x t") 'neotree-toggle)
(global-set-key (kbd "C-x C-n") 'neo-global--switch-to-buffer)
(setq neotree-hide-cursor t)
(setq neotree-toggle-window-keep-p t)


;; ;; Undo-tree
;; (use-package undo-tree
;;   :ensure t)

;; (global-undo-tree-mode)

;; Keybindings
;; M-o set to switch between windows
(global-set-key (kbd "M-o") 'other-window)

;; Nicer keybindings for windows management in God-mode
(global-set-key (kbd "C-x C-1") 'delete-other-windows)
(global-set-key (kbd "C-x C-2") 'split-window-below)
(global-set-key (kbd "C-x C-3") 'split-window-right)
(global-set-key (kbd "C-x C-0") 'delete-window)

;; IBuffer instead of Buffer List
(global-set-key (kbd "C-x C-b") 'ibuffer)
;; End Kybindings

;; Smex
(use-package smex
  :ensure t)

(global-set-key (kbd "M-x") 'smex)
(global-set-key (kbd "M-X") 'smex-major-mode-commands)

